<?php
/**
 * Provision new WordPress websites
 *
 */

// increase the default script execution timeout
set_time_limit(120);

// curl helper class
include "class-curl-request.php";

// global array to store non lethal errors
$all_errors = array();

// some other useful globals that might be better defined as constants
$nginx_config_dir_tmp = '/var/www/switchwp.com/provision/nginx_config';
$nginx_config_dir = '/etc/nginx/sites-available';
$provision_url = 'https://switchwp.com/provision/wp-provision.php';
$random_dev = '/dev/urandom';
$mysql_host = 'localhost';
$mysql_user = 'root';
$mysql_password = '';
$mysql_charset = 'utf8';
$mysql_collate = 'utf8_unicode_ci';
$wp_keysalts_url = 'https://api.wordpress.org/secret-key/1.1/salt/';
$web_server_root = '/var/www';
$wp_latest_tar_gz = '/var/www/switchwp.com/provision/latest.tar.gz';
$mkdir = '/bin/mkdir';
$tar = '/bin/tar';

$step = isset( $_GET['step'] ) ? (int) $_GET['step'] : 0;
switch($step) {
	case 0: // Step 1
	case 1: // Step 1, direct link.
		display_header();
		display_setup_form();
		display_footer();
		break;
	case 2:
		display_header();
		if ( !$domain = valid_domain( $_POST['domain'] ) ) {
			echo 'The domain is invalid. Try again?<br /><br />';
			display_setup_form();
			display_footer();
		}
		elseif ( domain_exists( $_POST['domain'] ) ) {
			echo 'Domain <b>' . $_POST['domain'] . '</b> exists. Cannot create this site. Try again?<br /><br />';
			display_setup_form();
			display_footer();
		}
		else {
			$db_info = create_db( $_POST['fname'], $_POST['lname'], $domain );
			create_wp_and_config( $db_info['dbname'], $db_info['dbuser'], $db_info['dbpass'], $_POST['domain'] );
			create_nginx_virtual_host( $_POST['domain'] );
			sleep(12);
			$passwd = random(21);
			$url = 'http://switchwp.com/wp-admin/install.php?step=2';
			$method = 'POST';
			$post_params = 'user_name=' . $_POST['fname'] .
							'&admin_password=' . $passwd .
							'&admin_password2=' . $passwd .
							'&admin_email=' . $_POST['email'] .
							// This could be another field.
							//'&blog_public=' . $_POST['blog_public'];
							'&blog_public=1';
			$resp = surf( $url, $_POST['domain'], $method, $post_params );
			if ( $resp['code'] != '200' ) {
				?><h1>Crap!</h1>
				<p>Failed to install WordPress.</p>
				<pre>Document Root: /var/www/<?php echo $_POST['domain'] ?><br />
				Database Name: <?php echo $db_info['dbname']; ?><br />
				Database User: <?php echo $db_info['dbuser']; ?><br />
				Web Server Virtual Host: <?php echo $_POST['domain']; ?>
				</pre>
				<?php
			}
			else {
			echo preg_replace( array( '~<\!DOCTYPE.*<h1>Success~sDx', '~<table.*</html>~sDx' ), array( '<h1>Success', '' ), $resp['body'] );
				?>
				<table class="form-table install-success">
				<tr>
					<th>Website URL</th>
					<td><p><em><a href="http://<?php echo $_POST['domain']; ?>" target="_blank">http://<?php echo $_POST['domain']; ?></a></em></p></td>
				</tr>
				<tr>
					<th>Admin Username</th>
					<td><p><em><?php echo $_POST['fname']; ?></em></p></td>
				</tr>
				<tr>
					<th>Admin Password</th>
					<td><p><em><?php echo $passwd; ?></em></p></td>
				</tr>
				<tr>
					<th>Customer's Email Address</th>
					<td><p><em><a href="mailto:<?php echo $_POST['email']; ?>"><?php echo $_POST['email']; ?></a></em></p></td>
				</tr>
				</table>
				<p class="step"><a href="http://<?php echo $_POST['domain']; ?>/wp-login.php" class="button" target="_blank">Log In</a></p>
				<?php
			}
			//log_entry( $db_info ); // Keep a log of every transaction.
			display_footer();
		}
		break;
}

/**
 * Functions
 *
 */

/**
 * Display the HTML header.
 *
 */
function display_header() {
	header( 'Content-Type: text/html; charset=utf-8' );
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel='stylesheet' id='install-css'  href='/wordpress/wp-admin/css/install.css?ver=3.4.2' type='text/css' media='all' />
	<title>WordPress Provisioning</title>
</head>
<body>
<h1 id="logo"><img alt="WordPress" src="/wordpress/wp-admin/images/wordpress-logo.png?ver=20120216" /></h1>

<?php
}

/**
 * Display the form.
 *
 */
function display_setup_form( $error = null ) {
?>
<form id="setup" method="post" action="/provision/index.php?step=2">
	<table class="form-table">
		<tr>
			<th align="right" scope="row"><label>Domain Name</label></th>
			<td><input name="domain" type="text" id="domain" size="64"></td>
		</tr>
		<tr>
			<th align="right" scope="row"><label>Customer's First Name</label></th>
			<td><input name="fname" type="text" id="fname" size="64"></td>
		</tr>
		<tr>
			<th align="right" scope="row"><label>Customer's Last Name</label></th>
			<td><input name="lname" type="text" id="lname" size="64"></td>
		</tr>
		<tr>
			<th align="right" scope="row"><label>Customer's Email Address</label></th>
			<td><input name="email" type="text" id="email" size="64"></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<p class="step"><input type="submit" name="Submit" value="Install WordPress" class="button" /></p>
			</td>		
		</tr>
	</table>
</form>
<?php
}


/**
 * Display the footer and any errors.
 *
 */
function display_footer() {
	global $all_errors;
	if ( isset( $all_errors[0] ) && $all_errors[0] != '' ) {
	?>
	<h2>Provisioning Errors</h2>
	<pre><?php var_dump( $all_errors ); ?></pre>
	<?php } ?>
	</body>
	</html>
	<?php
}


/**
 * Make sure the domain name entered is valid and if it is return
 * everything but the top level domain and trim everything after
 * 60 chars and add a random string to the end.
 *
 * @param string $domain is the domain name of the site to install.
 * @return the domain with 3 random alphanumeric chars appended.
 *
 */
function valid_domain($domain) {
    $ctr = 0;
	$subdomain = "";
	$pieces = explode(".", $domain);
	if ( ( $num_pieces = sizeof($pieces) ) < 2)
		return false;
    foreach($pieces as $piece) {
		$ctr++;
        if (!preg_match('/^[a-z\d][a-z\d-]{0,62}$/i', $piece) || preg_match('/-$/', $piece) )
            return false;
		else {
			$subdomain .= $piece . ".";
		}
		//if ( ($ctr + 1) == $num_pieces ) {
		if ( $ctr == $num_pieces ) {
			$subdomain = substr( $subdomain, 0, 58 );
			return $subdomain . random(3);
		}	
	}
}


/**
 * Return a random alphanumeric string
 *
 * @param int $len is the length of the random string to generate.
 * @return string containing the random string.
 *
 */
function random($len) {
	$chars = '/[0-9A-Za-z]/';
	$rs = "";
	$f=fopen('/dev/urandom', 'r');
	for($x = 0; $x < $len; $x++) {
		$c = fgetc($f);
		if (preg_match($chars, $c))
			$rs .= $c;
		else
			$x--;
	}
	fclose($f);
	return $rs;
}

/**
 * Make sure that the domain isn't already installed.
 *
 * @param string $domain is the domain name of the site being installed.
 * @return boolean letting the caller know if the domain exists.
 *
 */
function domain_exists( $domain ) {
	$dir = "/etc/nginx/sites-available/";
	$exists = false;
	if (is_dir($dir)) {
		if ($dh = opendir($dir)) {
			while (($file = readdir($dh)) !== false) {
				if ( $domain == $file )
					$exists = true;
			}
		}
        closedir($dh);
    }
	return $exists;
}

/**
 * Create the database
 *
 * @param string $lname is the last name of the customer.
 * @param string $dbname is the name to give the database using the domain name.
 * @return array containing the database, username and password.
 *
 */
function create_db( $fname, $lname, $dbname ) {
	global $mysql_password;
	$username = substr( $lname, 0, 12 ) . '_' . random(3);
	$password = random(21);
	$create_db_query = 'create database `' . $dbname . '` character set = utf8 collate = utf8_unicode_ci';
	$create_user_query = 'grant all privileges on `' . $dbname . '`.* to `' . $username . '`@localhost identified by \'' . $password . '\'';
	$dbh = new mysqli( 'localhost', 'root', $mysql_password );
	if ($dbh->connect_error) {
		printf("Connect failed: %s: %s\n", $dbh->connect_errno, $dbh->connect_error);
		exit();
	}
	if (!$dbh->query( $create_db_query ) ) {
		printf("Create DB error message: %s\n", $dbh->error);
		exit();
	}
	if (!$dbh->query( $create_user_query ) ) {
		printf("Create user error message: %s\n", $dbh->error);
		exit();
	}
	$dbh->close();
	return array( 'dbname' => $dbname, 'dbuser' => $username, 'dbpass' => $password );
}


/**
 * Create WordPress directory and wp_config.php.
 *
 * @param string $dbname is the name of the database.
 * @param string $dbuser is the user with access to the database.
 * @param string $dbpass is the database user's password.
 * @param string $domain is the domain of the new site.
 *
 */
function create_wp_and_config( $dbname='', $dbuser='', $dbpass='', $domain ) {
	$message = '';
	$host = 'localhost';
	$keysalt = 'https://api.wordpress.org/secret-key/1.1/salt/';
	// Get the keys and salts. Retry 3 times.
	for ( $x=0; $x<3; $x++ ) {
		$response = surf( $keysalt );
		if ( $response['code'] == '200' )
			break;
		else
			process_errors( "wp-provision.php->create_wp_and_config()\nElapsed time: ". $response['etime'] . "\nHTTP status: " . $response['code'] );
	}
	if ( $x == 3 )
		process_errors( "create_wp_and_config() -> ERROR: Failed getting salts and keys after 3 retries" );
	$keys = $response['body'];
	$config_text = <<<EOT
<?php
define('DB_NAME', '$dbname');
define('DB_USER', '$dbuser');
define('DB_PASSWORD', '$dbpass');
define('DB_HOST', 'localhost');
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', 'utf8_unicode_ci');

EOT;
	$config_text .= <<<'EOT'
$table_prefix = 'wp_';


EOT;

	$config_text .= <<<EOT
define('WP_POST_REVISIONS', false);
define('WPLANG', '');
define('WP_DEBUG', false);

// Authentication Unique Keys and Salts.
$keys

// DO NOT EDIT BELOW HERE
@ini_set('log_errors','On');
@ini_set('display_errors','Off');
@ini_set('memory_limit', '256M');
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
require_once(ABSPATH . 'wp-settings.php');
EOT;
	$shellout = '';
	$shellout = exec( '/bin/mkdir /var/www/' . $domain . ' 2>&1', $out, $status );
	if ( $status != 0 ) {
		$message .= "wp-provision.php->create_wp_and_config(): failed to create docroot for " . $domain . "\n";
		$message .= var_export( $out, true );
		process_errors( $message, true );
	}
	$shellout = exec( '/bin/tar xfz /var/www/switchwp.com/provision/latest.tar.gz -C /var/www/' . $domain . ' 2>&1', $out, $status );
	if ( $status != 0 ) {
		$message .= "wp-provision.php->create_wp_and_config(): failed to untar wordpress files for " . $domain . "\n";
		$message .= var_export( $out, true );
		process_errors( $message, true );
	}
	$wp_config = '/var/www/' . $domain . '/wordpress/wp-config.php';
	$fh = fopen( $wp_config, 'wb' ) or process_errors( "Failed to open wp-config.php for: " . $domain, true );
	fwrite( $fh, $config_text ) or process_errors( "Failed to write to wp-config.php for: " . $domain, true );
	fclose( $fh ) or process_errors( "Failed to close wp-config.php for: " . $domain, true );
}

/**
 * Create the nginx virtual host config file.
 *
 * @param string $domain containing the domain name of the new site.
 *
 */
function create_nginx_virtual_host( $domain ) {
	$config_text = <<<EOT
server {
	listen 80;
	server_name $domain;
	server_name www.$domain;
	root /var/www/$domain/wordpress;
	access_log /var/log/nginx/$domain.access.log;
	error_log /var/log/nginx/$domain.error.log;


EOT;

	$config_text .= <<<'EOT'
	# Deny all attempts to access hidden files such as .htaccess, .htpasswd, .DS_Store (Mac).
	location ~ /\. {
		deny all;
		access_log off;
		log_not_found off;
	}

	# Deny access to any files with a .php extension anywhere under /wp-content including
	# the  uploads directory
	location ~* /wp-content/.*.php$ {
		deny all;
	}

	location / {
		try_files $uri $uri/ /index.php?$args;
	}

	location ~ \.php$ {
		expires 600;
		# check to see if the visitor is logged in, a commenter,
		# or some other user who should bypass cache
		set $nocache "";
		if ($http_cookie ~ (comment_author_.*|wordpress_logged_in.*|wp-postpass_.*)) {
			set $nocache "Y";
		}
		include fastcgi_params;
		fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
		fastcgi_split_path_info ^(.+\.php)(/.+)$;
		fastcgi_index index.php;
		fastcgi_pass php5-fpm-unix-sock;

		# bypass cache if logged in.
		# Be sure that this is above all other fastcgi_cache directives
		fastcgi_no_cache $nocache;
		fastcgi_cache_bypass $nocache;

		# use stale cache if the backend fails.
		fastcgi_cache_use_stale error timeout invalid_header http_500;

EOT;

	$config_text .= <<<EOT
		# CACHE $domain
		fastcgi_cache $domain;
		fastcgi_cache_valid 200 302 1m;
		fastcgi_cache_valid 301 1h;
		fastcgi_cache_valid any 1m;
		fastcgi_cache_min_uses 1;
	}

	location = /favicon.ico {
		log_not_found off;
	}

	location = /robots.txt {
		allow all;
		log_not_found off;
	}

	location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
		expires max;
		log_not_found off;
	}

}
EOT;

	$nginx_config = '/var/www/switchwp.com/provision/nginx_config/' . $domain;
	$fh = fopen( $nginx_config, 'wb' ) or process_errors( "Failed to open file: " . $nginx_config, true );
	fwrite( $fh, $config_text ) or process_errors( "Failed to write to file: " . $nginx_config, true );
	fclose( $fh ) or process_errors( "Failed to close the file: " . $nginx_config, true );
}

/**
 * Connect to other websites. Used for completing the WP install and
 * getting keys and salts.
 *
 * @param string $url is the full URL containing the hostname and request.
 * @param string $host is the hostname of the new site used in the Host: HTTP header.
 * @param string $method is the HTTP method (GET | POST).
 * @param string $post_params are the POST arguments in key=value&key=value format.
 *
 * @return array containing HTTP response code, the body of the response, elapsed time.
 */
function surf( $url, $host='', $method='GET', $post_params='' ) {
	try {
		$mych = new CurlRequest;
		$params = array( 'url' => $url,
						'host' => $host,
						'header' => '',
						'method' => $method,
						'referer' => '',
						'cookie' => '',
						'post_fields' => $post_params,
						'timeout' => 90,
						'verbose' => 0 );

		$mych->init( $params );
		$result = $mych->exec();
		if ( $result['curl_error'] ) throw new Exception( $result['curl_error'] );
		if ( $result['http_code'] != '200' ) throw new Exception( "HTTP Code = " . $result['http_code'] . "\nBody: " . $result['body'] );
		if ( !$result['body'] ) throw new Exception( "Body of file is empty" );
		//echo $result['header'];
		//echo 'HTTP return code: ' . $result['http_code'];
	}
	catch ( Exception $e ) {
		process_errors( "wp-provision.php->surf(): " . $e->getMessage() );

	}
	return array( 'code' => $result['http_code'], 'body' => $result['body'], 'etime' => $result['etime'] );
}

/**
 * Process errors
 *
 * @param string $message is the error message to write to the log and/or screen.
 * @param boolean $exit_now will determine whether or not to terminate the process.
 * 
 */
function process_errors( $message, $exit_now = false ) {
	global $all_errors;
	$all_errors[] = $message;
	error_log( $message );
	if ( $exit_now )
		exit( '<pre>' . var_export( $all_errors, true ) . '</pre>' );
}

?>
