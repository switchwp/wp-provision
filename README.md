wp-provision.php creates a new WordPress website via an
online web form.

The check_nconfigs.sh script is executed every n seconds
(currently 10) and checks for the existence of nginx config
files that have been created by the wp-provision.php script.
It copies config files to nginx config directories, adds the
required cache directives and reloads the nginx configuration.
