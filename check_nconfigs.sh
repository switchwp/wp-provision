#!/bin/bash
CONFS=/var/www/switchwp.com/provision/nginx_config
cd $CONFS
CF=`/usr/bin/find . -type f -exec /usr/bin/basename {} \;`
if [ "$CF" != "" ]; then
	SA=/etc/nginx/sites-available
	SE=/etc/nginx/sites-enabled
	NX=/etc/nginx
	for f in $CF; do
		mv $f $SA
		cd $SE
		ln -s ../sites-available/$f
		CD=`/bin/grep CACHE $f | /bin/sed -e 's/^.*# CACHE //'`
		if [ "$CD" != "" ]; then
			/bin/echo "fastcgi_cache_path /var/cache/nginx/$CD levels=1:2 keys_zone=$CD:50m inactive=5m;" >> $NX/cache.conf
		fi
		cd $CONFS
	done
	if /usr/sbin/nginx -s reload; then
		echo "The following sites added to nginx:"
		echo $CF
	else
		echo "Config error while loading the following sites into nginx:"
		echo $CF
	fi
	exit 1
else
	exit 0
fi
